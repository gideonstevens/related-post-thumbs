This code snippet connects with Jetpack's Related Post and returns post ids. It then pulls just the thumbnails to display.
When it grows up this snippet wants to be a Wordpress Plugin.

At the moment, copy the code file to your functions file, and copy the widget to a text box.